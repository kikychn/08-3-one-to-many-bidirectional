package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// TODO
//
// 请使用双向映射定义 ParentEntity 和 ChildEntity 的 one-to-many 关系。其中 ParentEntity
// 的数据表应当为如下的结构。
//
// parent_entity
// +─────────+──────────────+──────────────────────────────+
// | Column  | Type         | Additional                   |
// +─────────+──────────────+──────────────────────────────+
// | id      | bigint       | primary key, auto_increment  |
// | name    | varchar(20)  | not null                     |
// +─────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ParentEntity {
    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @Column(nullable = false, length = 20)
    private String name;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<ChildEntity> children = new ArrayList<>();

    public ParentEntity() {
    }

    public ParentEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void removeChild(ChildEntity child) {
        child.setParent(null);
    }

    public List<ChildEntity> getChildren() {
        return children;
    }

    public void setChildren(List<ChildEntity> children) {
        this.children = children;
    }

    public void addChild(ChildEntity child) {
        child.setParent(this);
        children.add(child);
    }
}
// --end->